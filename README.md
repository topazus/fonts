
- [Anonymous Pro](https://www.marksimonson.com/fonts/view/anonymous-pro)

Anonymous Pro contains embedded bitmaps for smaller sizes,but Anonymous Pro Minus does not.

- [Courier Prime](https://github.com/quoteunquoteapps/CourierPrime)

- [Courier Prime Code](https://github.com/quoteunquoteapps/CourierPrimeCode)

Gitronic and MonoLisa font were downloaded from https://github.com/bad33bug/coding-fonts

- [Comic Shanns](https://github.com/shannpersand/comic-shanns)

- [Lab Mono](https://github.com/hatsumatsu/Lab-Mono)

- [Menlo](https://github.com/ueaner/fonts.git)

- [Mensch](https://robey.lag.net/2010/06/21/mensch-font.html)

- Monaco

```
wget http://jorrel.googlepages.com/Monaco_Linux.ttf
```

- [Office Code Pro](https://github.com/nathco/Office-Code-Pro)

- [Overpass Mono](https://github.com/RedHatOfficial/Overpass)

#### Pragmata Pro

Pragamata Pro fonts downloaded from https://github.com/aemi-dev/Fonts

the `CascadiaCode.ttf` file is the same as the files in `cascadia-code/` folder.

install fonts

```sh
git clone https://topazus@bitbucket.org/topazus/fonts.git
cp -r fonts ~/.local/share
fc-cache -fv
```
